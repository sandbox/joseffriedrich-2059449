<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<title>Google Maps JavaScript API v3 Example: Info Window Simple</title>
<style>
body {
  color: #666;
  font: normal 81.3%/1.538em "lucida grande", tahoma, verdana, arial, sans-serif;
  line-height: 14px;
}
</style>
<link href="http://code.google.com/apis/maps/documentation/javascript/examples/default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
  function initialize() {


    var locations = [
            {
                "title": "Waschsalon Trommelwirbel",
                "www": "http://www.trommelwirbel.de/index.php",
                "longitude": "11.091126",
                "latitude": "49.460182",
                "street": "Bayreuther Straße 21",
                "file": "01_Trommelwirbel_Patrick_Streit",
                "description": "Wer die traditionell biederen Waschsalons satt hat, ist in der Wäscherei Trommelwirbel genau richtig. Der dazu servierte, schmackhafte Kaffee und die 70er-Jahre Musik machen einen Besuch zu einem unvergesslichen Erlebnis.",

            },
            {
                "title": "KILL-O-ZAP - music & more",
                "www": "http://www.kill-o-zap.de",
                "longitude": "11.075372",
                "latitude": "49.457298",
                "street": "Obere Schmiedgasse 38",
                "file": "02_KillOZap_Dominic_Zunner",
                "description": "Music and More im 60er-Jahre-Design. Kill-O-Zap - Ein Wohnzimmer zum Kaufen.",
            },
            {
                "title": "14/80",
                "www": "http://www.in-goho.de/index.php?id=24",
                "longitude": "11.053998",
                "latitude": "49.449173",
                "street": "Kernstraße 32",
                "file": "03_14,80_Sophia_Bauer",
                "description": "Wer einen entspannten Nachmittag zwischen antiken, einzigartigen Gegenständen und humorvollen Leuten verbringen will, ist im Vintage-Laden 14,80 in Gostenhof herzlichst willkommen. Es gibt nahezu alles, was das Herz begehrt und einmal eingetreten, findet man sicher etwas Passendes für jeden Anlass!",
            },
            {
                "title": "Auguste",
                "www": "http://www.premium-junkfood.de",
                "longitude": "11.094397",
                "latitude": "49.441284",
                "street": "Augustenstraße 37",
                "file": "04_Auguste_Michael_Stecher",
                "description": "Die Auguste, das heißt leckeres Junkfood kombiniert mit gutem Gewissen.",
            },
            {
                "title": "Roxy-Fremdsprachenkino",
                "www": "http://www.roxy-nuernberg.de",
                "longitude": "11.084091",
                "latitude": "49.409138",
                "street": "Julius-Loßmann-Straße 116",
                "file": "05_Roxy_Kino_Jonas_Volkert",
                "description": "Roxy Kino - Das Roxy ist eines der letzten Kinos der alten Schule und ein reiner Familienbetrieb. Da der Besitzer in den wohlverdienten Ruhestand eintreten möchte, schien das Schicksal des Kinos bis vor kurzem ungewiss.",
            },
            {
                "title": "Hutmuseum Brömme",
                "www": "http://www.hut-broemme.de",
                "longitude": "11.083190",
                "latitude": "49.455978",
                "street": "Innere Laufer Gasse 33",
                "file": "06_Hutmuseum_Broemme_Christopher_Claussen",
                "description": "Das Hutmuseum Brömme ist nicht nur einfach ein gutes Museum. Man kann hier auch alles über Hüte lernen und Hüte aller Arten kaufen.",
            },
            {
                "title": "Café Violetta",
                "www": "http://www.la-violetta.de/violetta_flash.html",
                "longitude": "11.073775",
                "latitude": "49.452419",
                "street": "Obere Wörthstraße 10",
                "file": "07_Cafe_Violetta_Philip_Sapke",
                "description": "Auffallend anders - Das Café Violetta etwas versteckt und abseits, aber doch im Herzen unserer Stadt, besticht es mit seinem Charme.",
            },
            {
                "title": "Bäckerei Gabsteiger",
                "www": "http://www.in-goho.de/index.php?id=28",
                "longitude": "11.054636",
                "latitude": "49.450611",
                "street": "Kernstraße 7",
                "file": "08_Gabsteiger_Baeckerei_Philipp_Yuerue",
                "description": "Eine Bäckerei wie jede andere? Nicht ganz, die familiäre Atmosphäre in Zeiten der auswuchernden Bäckerei-Ketten sucht man anderswo vergebens.",
            },
            {
                "title": "Kunstverein",
                "www": "http://www.kunstverein-nuernberg.de/kunstverein",
                "longitude": "11.094032",
                "latitude": "49.430004",
                "street": "Frankenstraße 200",
                "file": "09_KV_Alexander_Klosa",
                "description": "Der Kunstverein, angesiedelt im Z-Bau an der Frankenstraße, ist die perfekte Adresse für jeden, der nicht auf Mainstream-Clubs steht. Neben Partys und Konzerten gibt es auch viele andere Möglichkeiten, gemeinsam Zeit zu verbringen……",
            },
            {
                "title": "Café Express",
                "www": "http://www.cafe-express.de",
                "longitude": "11.082923",
                "latitude": "49.442150",
                "street": "Bulmannstraße 4",
                "file": "10_Cafe_Express_Anastasiya_Morozova",
                "description": "Das Café Express - Jazz, Schach, Dart, Unterhaltung und Diskussionen. Seit 1992 ein wunderbarer Ort zum Entspannen.",
            },
            {
                "title": "Gregor Samsa",
                "www": "http://www.franken-wiki.de/index.php/Gregor_Samsa",
                "longitude": "11.088684",
                "latitude": "49.463276",
                "street": "Maxfeldstraße 79",
                "file": "11_Gregor_Samsa_Jakob_Diem",
                "description": "Die älteste Künstlerkneipe Nürnbergs befindet sich – gut versteckt – in der Maxfeldstraße 79. Hier warten unzählige Gulaschvariationen auf den geneigten Hungerkünstler.",
            },
            {
                "title": "Fachmarie - die Glücksboutique",
                "www": "http://www.fachmarie.de",
                "longitude": "11.056440",
                "latitude": "49.450932",
                "street": "Fürther Straße 50",
                "file": "12_Fachmarie_Anna_Maly",
                "description": "Dieser Laden bietet individuelle Möglichkeiten für den Verkauf von selbstgemachter Ware und ist ein MUSS für alle kreativen Ladies! Die Fachmarie ist die perfekte Anlaufstelle für kleine, hübsche Präsente.",
            },

        ];

    var locLatLon = [];
    var contentString = [];

    for(i=0; i<locations.length; i++) {

      locLatLon[i] = new google.maps.LatLng(locations[i].latitude,locations[i].longitude);

      contentString[i] = '<h1>'+locations[i].title+'</h1>'+
          '<p><audio controls="controls">'+
            '<source src="http://pirckheimer-gymnasium.dev/sites/default/files/radio_ag_lucky_13/'+locations[i].file+'.mp3" type="audio/mp3">'+
            '<source src="http://pirckheimer-gymnasium.dev/sites/default/files/radio_ag_lucky_13/'+locations[i].file+'.ogg" type="audio/ogg">'+
            'Ihr Browser unterstützt das Audio-Tag nicht.'+
          '</audio></p>'+

          '<p>'+locations[i].description+'</p>'+
          '<p><strong>Adresse: </strong>'+locations[i].street+'<br>'+
          '<strong>Homepage: </strong><a href="'+locations[i].www+'" target="_blank">'+locations[i].www+'</a></p>';

    };

    var mapOptions = {
      zoom: 12,
      center: locLatLon[0],

      mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

    var marker = [];
    var infowindow = null;

    infowindow = new google.maps.InfoWindow({
      maxWidth: 400
    });



    for(i=0; i<locations.length; i++) {

      marker[i] = new google.maps.Marker({
          position: locLatLon[i],
          map: map,
          title: locations[i].title,
          html: contentString[i],
      });

      google.maps.event.addListener(marker[i], 'click', function() {
        infowindow.setContent(this.html);
        infowindow.open(map, this);
      });

    };

  }

</script>
</head>
<body onload="initialize()">
  <div id="map_canvas"></div>
</body>
</html>
